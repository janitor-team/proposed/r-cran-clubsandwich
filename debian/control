Source: r-cran-clubsandwich
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-clubsandwich
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-clubsandwich.git
Homepage: https://cran.r-project.org/package=clubSandwich
Standards-Version: 4.6.1
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-sandwich
Testsuite: autopkgtest-pkg-r

Package: r-cran-clubsandwich
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R cluster-robust (Sandwich) variance estimators with small-sample
 Corrections Provides several cluster-robust variance estimators
 (i.e., sandwich estimators) for ordinary and weighted least
 squares linear regression models, including the bias-reduced
 linearization estimator introduced by Bell and McCaffrey (2002)
 <http://www.statcan.gc.ca/pub/12-001-x/2002002/article/9058-eng.pdf>
 and developed further by Pustejovsky and Tipton (2017)
 <DOI:10.1080/07350015.2016.1247004>. The package includes
 functions for estimating the variance- covariance matrix and for
 testing single- and multiple- contrast hypotheses based on Wald
 test statistics. Tests of single regression coefficients use
 Satterthwaite or saddle-point corrections. Tests of multiple-contrast
 hypotheses use an approximation to Hotelling's T-squared
 distribution. Methods are provided for a variety of fitted models,
 including lm() and mlm objects, glm(), ivreg() (from package
 'AER'), plm() (from package 'plm'), gls() and lme() (from 'nlme'),
 robu() (from 'robumeta'), and rma.uni() and rma.mv() (from
 'metafor').
